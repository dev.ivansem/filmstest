package com.ivansem.films

import android.app.Application
import android.content.Context
import com.ivansem.films.dagger.AppMainComponent
import com.ivansem.films.dagger.DaggerAppMainComponent

class App: Application() {


    override fun onCreate() {
        super.onCreate()
        injector = DaggerAppMainComponent.builder().build()
        context = this
    }

    companion object {
        const val TAG = "#films"
        lateinit var injector: AppMainComponent
        lateinit var context: Context
    }
}