package com.ivansem.films

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.ivansem.films.films.FilmsFlowFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager
            .beginTransaction()
            .add(R.id.container, FilmsFlowFragment())
            .commit()
    }
}
