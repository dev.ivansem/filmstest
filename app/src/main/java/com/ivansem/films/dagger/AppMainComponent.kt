package com.ivansem.films.dagger

import com.ivansem.films.dagger.modules.DbModule
import com.ivansem.films.dagger.modules.RestModule
import com.ivansem.films.dagger.modules.RetrofitModule
import com.ivansem.films.films.presenter.FilmsListPresenter
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [RetrofitModule::class, RestModule::class, DbModule::class])
interface AppMainComponent {
    fun inject(presenter: FilmsListPresenter)
}