package com.ivansem.films.dagger.modules

import android.arch.persistence.room.Room
import android.util.Log
import com.ivansem.films.App
import com.ivansem.films.db.AppDatabase
import com.ivansem.films.db.FilmsDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DbModule {
    @Provides
    @Singleton
    fun providesFilmsDao(db: AppDatabase): FilmsDao {
        return db.filmsDao()
    }

    @Provides
    @Singleton
    fun providesDb(): AppDatabase {
        return Room.databaseBuilder(
            App.context,
            AppDatabase::class.java, "films.db"
        ).build()
    }
}