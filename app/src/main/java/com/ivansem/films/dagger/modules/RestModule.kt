package com.ivansem.films.dagger.modules

import com.ivansem.films.net.FilmsApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class RestModule {
    @Singleton
    @Provides
    fun providesFilmsApi(retrofit: Retrofit) = retrofit.create(FilmsApi::class.java)

    companion object {
        const val API_KEY = "befc7872520fd736c58948abb2f4a53c"
    }
}