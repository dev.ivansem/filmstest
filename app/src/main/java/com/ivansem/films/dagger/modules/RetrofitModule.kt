package com.ivansem.films.dagger.modules

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class RetrofitModule {
    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson): Retrofit {
        val okBuilder = OkHttpClient.Builder()
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)

        return Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okBuilder.build())
                .baseUrl(HTTP_URL)
                .build()
    }

    @Singleton
    @Provides
    fun provideGson() = GsonBuilder()
            .setLenient()
            .create()

    companion object {
        const val HTTP_URL = "http://api.themoviedb.org"
        const val IMAGE_URL = "https://image.tmdb.org/t/p/w500"
        private const val READ_TIMEOUT = 10L
        private const val WRITE_TIMEOUT = 10L
        private const val CONNECT_TIMEOUT = 10L
    }
}