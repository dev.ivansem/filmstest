package com.ivansem.films.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

@Database(entities = [DbFilm::class], version = 1)
abstract class AppDatabase: RoomDatabase() {
    abstract fun filmsDao(): FilmsDao
}