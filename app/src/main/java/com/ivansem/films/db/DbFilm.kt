package com.ivansem.films.db

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "film")
data class DbFilm(
    @PrimaryKey var title: String,
    var posterPath: String,
    var overview: String
)