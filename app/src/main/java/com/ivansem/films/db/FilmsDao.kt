package com.ivansem.films.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import io.reactivex.Single

@Dao
interface FilmsDao {
    @Query("select * from film;")
    fun getAll(): Single<List<DbFilm>>

    @Insert
    fun insert(films: List<DbFilm>)

    @Delete
    fun delete(films: List<DbFilm>)
}