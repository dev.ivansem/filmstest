package com.ivansem.films.films

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ivansem.films.R
import com.ivansem.films.films.view.FilmsListFragment

class FilmsFlowFragment: Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fr_flow_films, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (savedInstanceState == null) {
            fragmentManager!!.beginTransaction()
                .replace(R.id.flow_films_container, FilmsListFragment())
                .commit()
        }
    }
}