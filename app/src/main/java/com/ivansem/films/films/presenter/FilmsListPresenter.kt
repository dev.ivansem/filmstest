package com.ivansem.films.films.presenter

import android.text.format.DateUtils
import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.ivansem.films.App
import com.ivansem.films.db.DbFilm
import com.ivansem.films.db.FilmsDao
import com.ivansem.films.films.view.IFilmsView
import com.ivansem.films.model.Film
import com.ivansem.films.net.FilmsApi
import com.ivansem.films.utils.Prefs
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@InjectViewState
class FilmsListPresenter: MvpPresenter<IFilmsView>() {
    @Inject
    lateinit var filmsApi: FilmsApi

    @Inject
    lateinit var filmsDao: FilmsDao

    val compositeDisposable = CompositeDisposable()

    init {
        App.injector.inject(this)
    }

    fun loadFilms() {
        compositeDisposable.add(
            loadFromNetOrCache()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    viewState.filmsLoaded(it)
                }, { e -> Log.e(App.TAG, e.message, e) })
        )
    }

    private fun loadFromNetOrCache(): Single<List<Film>> {
        return if (System.currentTimeMillis() - Prefs.getLong(KEY_CACHE_SAVED_TIME) < CACHED_TIME) {
            filmsDao.getAll().map { dbFilms -> dbFilms.map {
                Film(it.posterPath, it.title, it.overview)
            }}.flatMap { cachedFilms ->
                if (cachedFilms.isEmpty()) {
                    loadFromNet()
                } else {
                    Single.just(cachedFilms)
                }
            }
        } else {
            loadFromNet()
        }
    }

    private fun loadFromNet(): Single<List<Film>> {
        return filmsApi.getFilms()
            .flatMap { films ->
                upsertFilms(films.results).toSingle { films.results }
            }
    }

    private fun upsertFilms(films: List<Film>): Completable {
        val dbFilms = films.map { film ->
            DbFilm(film.title, film.posterPath, film.overview)
        }
        return Completable.fromCallable {
            filmsDao.delete(dbFilms)
            filmsDao.insert(dbFilms)
            Prefs.saveLong(KEY_CACHE_SAVED_TIME, System.currentTimeMillis())
        }
    }

    companion object {
        const val KEY_CACHE_SAVED_TIME = "KEY_CACHE_SAVED_TIME"
        const val CACHED_TIME = DateUtils.HOUR_IN_MILLIS * 2
    }
}