package com.ivansem.films.films.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.ivansem.films.R
import com.ivansem.films.dagger.modules.RetrofitModule
import com.ivansem.films.model.Film
import kotlinx.android.synthetic.main.fr_film_detail.*

class FilmDetailFragment: Fragment() {
    var film: Film? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fr_film_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        film?.let {
            tv_film_detail_title.text = it.title
            tv_film_detail_overview.text = it.overview
            Glide.with(this).load(RetrofitModule.IMAGE_URL + it.posterPath).centerCrop().into(iv_film_detail_poster)
        }
    }

    companion object {
        fun getInstance(film: Film): Fragment {
            val fr = FilmDetailFragment()
            fr.film = film
            return fr
        }
    }
}