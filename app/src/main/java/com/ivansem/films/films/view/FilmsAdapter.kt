package com.ivansem.films.films.view

import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.ivansem.films.R
import com.ivansem.films.dagger.modules.RetrofitModule
import com.ivansem.films.model.Film

class FilmsAdapter(val films: List<Film>): RecyclerView.Adapter<FilmsAdapter.FilmsViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilmsViewHolder {
        return FilmsViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_film, parent, false))
    }

    override fun getItemCount(): Int {
        return films.size
    }

    override fun onBindViewHolder(vh: FilmsViewHolder, position: Int) {
        vh.bind(films[position])
    }

    inner class FilmsViewHolder(view: View): RecyclerView.ViewHolder(view) {
        private val ivPoster = itemView.findViewById<ImageView>(R.id.iv_poster)
        private val tvName = itemView.findViewById<TextView>(R.id.tv_film_name)

        fun bind(film: Film) {
            tvName.text = film.title
            Glide.with(itemView).load(RetrofitModule.IMAGE_URL + film.posterPath).centerCrop().into(ivPoster)
            itemView.setOnClickListener {
                val frManager = (itemView.context as AppCompatActivity).supportFragmentManager
                frManager.beginTransaction()
                    .replace(R.id.flow_films_container, FilmDetailFragment.getInstance(film))
                    .addToBackStack(FilmDetailFragment::class.java.simpleName)
                    .commit()
            }
        }
    }
}