package com.ivansem.films.films.view

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.ivansem.films.R
import com.ivansem.films.films.presenter.FilmsListPresenter
import com.ivansem.films.model.Film
import kotlinx.android.synthetic.main.fr_films_list.*

class FilmsListFragment: MvpAppCompatFragment(), IFilmsView {
    @InjectPresenter
    lateinit var presenter: FilmsListPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fr_films_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rv_films_list.layoutManager = LinearLayoutManager(context)
        presenter.loadFilms()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.compositeDisposable.dispose()
    }

    override fun filmsLoaded(films: List<Film>) {
        rv_films_list.adapter = FilmsAdapter(films)
    }
}