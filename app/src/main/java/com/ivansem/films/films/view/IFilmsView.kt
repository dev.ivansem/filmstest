package com.ivansem.films.films.view

import com.arellomobile.mvp.MvpView
import com.ivansem.films.model.Film

interface IFilmsView : MvpView {
    fun filmsLoaded(films: List<Film>)
}