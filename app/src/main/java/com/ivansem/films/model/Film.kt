package com.ivansem.films.model

import com.google.gson.annotations.SerializedName

data class Film(
    @SerializedName("poster_path")
    val posterPath: String,
    val title: String,
    val overview: String
)