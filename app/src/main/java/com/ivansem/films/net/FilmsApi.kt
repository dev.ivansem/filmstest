package com.ivansem.films.net

import com.ivansem.films.dagger.modules.RestModule
import com.ivansem.films.net.response.FilmsResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface FilmsApi {
    @GET("3/movie/popular")
    fun getFilms(
        @Query("api_key") apiKey: String = RestModule.API_KEY
    ): Single<FilmsResponse>
}