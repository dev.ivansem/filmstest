package com.ivansem.films.net.response

import com.ivansem.films.model.Film

data class FilmsResponse(val results: ArrayList<Film>)
