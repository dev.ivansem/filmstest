package com.ivansem.films.utils

import android.content.Context
import com.ivansem.films.App

object Prefs {
    const val PREFS_NAME = "APP_PREFS"
    val prefs = App.context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

    fun getLong(key: String): Long {
        return prefs.getLong(key, 0L)
    }

    fun saveLong(key: String, value: Long) {
        prefs.edit().putLong(key, value).apply()
    }
}